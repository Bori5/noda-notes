const express = require('express');
const notesController = require('./notesController');
const {authMiddleware} = require('../auth/authMiddleware');

const router = express.Router();

router.get('/', authMiddleware, notesController.get);

router.get('/:id', authMiddleware, notesController.getById);

router.post('/', authMiddleware, notesController.create);

router.put('/:id', authMiddleware, notesController.update);

router.patch('/:id', authMiddleware, notesController.toggle);

router.delete('/:id', authMiddleware, notesController.delete);

module.exports = router;
