const Model = require('./notesModel');
const Joi = require('joi');

const joiSchema = Joi.object({
  text: Joi.string().required(),
});

module.exports = class NotesController {
  static async get({user: {_id: userId}, query: {limit, offset}}, res) {
    try {
      const notes = await Model.find({userId}, {}, {
        limit: +limit,
        skip: +offset,
        sort: {createdDate: -1},
      });

      res.status(200).json({notes});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async create({user: {_id: userId}, body: {text}}, res) {
    try {
      await joiSchema.validateAsync({text});
      const note = new Model({userId, text});
      await note.save();
      res.json({message: 'Success'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async getById({user: {_id: userId}, params: {id}}, res) {
    try {
      const note = await Model.findOne({_id: id, userId});
      res.status(200).json({note});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async update({user: {_id: userId}, params: {id}, body: {text}}, res) {
    try {
      await joiSchema.validateAsync({text});
      await Model.updateOne({_id: id, userId}, {text});
      res.json({message: 'Success'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async toggle({user: {_id: userId}, params: {id}}, res) {
    try {
      const note = await Model.findOne({_id: id, userId});
      await Model.updateOne({_id: id}, {completed: !note['completed']});

      res.status(200).json({message: `toggled ${!note['completed']}`});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async delete({user: {_id: userId}, params: {id}}, res) {
    try {
      await Model.findOneAndDelete({_id: id, userId});
      res.status(200).json({message: `note was deleted`});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }
};
