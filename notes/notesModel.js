const {model, Schema} = require('mongoose');

const noteSchema = new Schema({
  userId: {
    type: String,
    require: true,
  },
  text: {
    type: String,
    require: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
}, {versionKey: false});

module.exports = model('Note', noteSchema);
