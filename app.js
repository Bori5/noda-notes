const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const express = require('express');
const logger = require('morgan');
const path = require('path');
const cors = require('cors');
const config = require('./config/common');
const {user: dbUser, password: dbPass, name: dbName} = config[config.env].db;

const apiAuth = require('./auth/authApi');
const apiUsers = require('./users/usersApi');
const apiNotes = require('./notes/notesApi');
const indexRouter = require('./routes/index');

const app = express();

app.use(logger('short'));
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());
app.use('/', indexRouter);
app.use('/api/auth', apiAuth);
app.use('/api/notes', apiNotes);
app.use('/api/users/me', apiUsers);
app.use((req, res, next) => {
  res.status(404).json({reason: 'not found'});
});
/**
 * Express default error handler
 */
app.use((err, req, res, next) => {
  res.status(500).json({reason: 'something went wrong ¯\\_(ツ)_/¯'});
  console.error(err.message);
});

(async () => {
  try {
    await mongoose.connect(`mongodb+srv://${dbUser}:${dbPass}@clusteri0.eyttu.mongodb.net/${dbName}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
  } catch (e) {
    throw e;
  }
})();

module.exports = app;
