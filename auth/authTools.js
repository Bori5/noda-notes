const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config/common');

const {saltRounds} = config[config.env].password;

const isPasswordMatch = (password, hash) => bcrypt.compare(password, hash);

const createPasswordHash = (password) => bcrypt.hash(password, saltRounds);

const createUserToken = ({_id, username}) => new Promise((resolve, reject) => {
  jwt.sign({_id, username}, config.JWT_SECRET, (error, token) => {
    if (error) reject(error);
    resolve(token);
  });
});

module.exports = {
  isPasswordMatch,
  createUserToken,
  createPasswordHash,
};
