const {create: createUser, getByName} = require('../users/usersController');
const authTool = require('./authTools');
const Joi = require('joi');

const joiSchema = Joi.object({
  username: Joi.string().required().pattern(new RegExp('^[a-zA-Z0-9]{2,30}$')),
  password: Joi.string().required().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
});

module.exports = class AuthController {
  static async register({body: {username, password}}, res) {
    try {
      await joiSchema.validateAsync({username, password});
      await createUser(username, await authTool.createPasswordHash(password));
      res.json({message: 'Success'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async login({body: {username, password}}, res) {
    try {
      await joiSchema.validateAsync({username, password});
      const user = await getByName(username);

      if (!user || !(await authTool.isPasswordMatch(password, user.password))) {
        return res.status(400).json({message: `Wrong username or password!`});
      }

      res.json({message: 'Success', jwt_token: await authTool.createUserToken(user)});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }
};
