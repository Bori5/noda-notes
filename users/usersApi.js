const {authMiddleware} = require('../auth/authMiddleware');
const userController = require('./usersController');
const express = require('express');
const router = express.Router();

router.get('/', authMiddleware, userController.get);

router.patch('/', authMiddleware, userController.changePassword);

router.delete('/', authMiddleware, userController.delete);

module.exports = router;
