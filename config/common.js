module.exports = {
  env: process.env.NODE_ENV || 'development',
  JWT_SECRET: process.env.JWT_SECRET,

  development: {
    db: {
      user: 'master',
      password: 'mastercard',
      name: 'notes',
    },
    password: {
      saltRounds: 2,
    },
  },
  production: {
    db: {
      user: 'user',
      password: 'won\'t tell you',
      name: 'notes',
    },
    password: {
      saltRounds: 10,
    },
  },
};
